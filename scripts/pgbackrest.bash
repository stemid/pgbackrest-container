#!/usr/bin/env bash

set -eux

# Expand non-matching file globs to zero instead of 1
shopt -s nullglob

function wait_for_postgres() {
  countMax=$1 && shift
  count=0
  while ! pg_isready; do
    sleep 5
    (( count++ ))
    if [ $count -gt $countMax ]; then
      exit 1
    fi
  done
}

# Borrow these from pgbackrest environment to avoid setting them twice
export AWS_DEFAULT_REGION=$PGBACKREST_REPO1_S3_REGION
export AWS_ACCESS_KEY_ID=$PGBACKREST_REPO1_S3_KEY
export AWS_SECRET_ACCESS_KEY=$PGBACKREST_REPO1_S3_KEY_SECRET

endpointUrl=''
if [ -n "$AWS_S3_ENDPOINT_URL" ]; then
  endpointUrl="--endpoint-url=$AWS_S3_ENDPOINT_URL"
fi

numBackups=0
numObjects=$(aws "$endpointUrl" s3api list-objects-v2 --bucket="$PGBACKREST_REPO1_S3_BUCKET" --prefix=pgbackrest | jq '.Contents | length')
test $? -ne 0 && exit $?

# No need to match .* files in this case
pgdataFiles=("${PGDATA:-/var/lib/postgresql/data}"/*)

# Objects in S3 exist, check number of backups.
if [ ${numObjects:-0} -gt 1 ]; then
  numBackups=$(pgbackrest --output=json info | jq '.[0].backup | length')
  pgbackrestReturn=$?
  test $pgbackrestReturn -ne 0 && exit $pgbackrestReturn
fi

if [ "$1" = 'restore' ]; then
  # Backup exists, but empty PGDATA dir; restore from backup.
  if [[ ${#pgdataFiles[@]} -eq 0 && $numBackups -gt 0 ]]; then
    pgbackrest restore && exit $?
  fi

  # Non-empty PGDATA but override flag set; delete files and restore backup.
  if [[ ${#pgdataFiles[@]} -gt 0 && ${PGBACKREST_OVERRIDE_RESTORE:-'n'} = 'y' ]]; then
    find "$PGDATA" -type f -delete
    pgbackrest restore && exit $?
  fi

  # Else do nothing and start postgresql normally
fi

# Postgresql must be running for this to work
if [ "$1" = 'backup' ]; then
  # Wait a while for postgresql to get ready
  wait_for_postgres 10

  # No backup exists, PGDATA is not empty; create stanza. Should only run
  # first time.
  if [[ $numBackups -eq 0 && ${#pgdataFiles[@]} -gt 0 ]]; then
    pgbackrest stanza-create
    pgbackrest check
  fi

  # By default pgbackrest will attempt an incremental backup and fall back
  # to full backup if no full backup exists.
  pgbackrest backup
fi

# Force full backup
if [ "$1" = 'full-backup' ]; then
  wait_for_postgres 5

  if [[ $numBackups -eq 0 && ${#pgdataFiles[@]} -gt 0 ]]; then
    pgbackrest stanza-create
    pgbackrest check
  fi

  pgbackrest --type=full backup
fi
