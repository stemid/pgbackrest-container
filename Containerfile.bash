#!/usr/bin/env bash

set -xeuo pipefail

pgVersion=${postgresVersion:-16}
ctr=$(buildah from docker.io/postgres:$postgresVersion)

buildah run "$ctr" -- sh -c 'apt update && apt install pgbackrest awscli jq -y'
buildah run "$ctr" -- mkdir -p /etc/postgresql/conf.d
buildah copy --chmod 0755 "$ctr" scripts/pgbackrest.bash /usr/bin/pgbackrest.bash
buildah copy --chmod 0644 "$ctr" postgresql.conf /etc/postgresql/postgresql.conf
buildah commit "$ctr" "localhost/${CI_PROJECT_NAME:-pgbackrest}:latest"
buildah commit "$ctr" "localhost/${CI_PROJECT_NAME:-pgbackrest}:$pgVersion"
