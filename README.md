# pgBackRest container

Extends docker.io/postgres to add pgbackrest, and a script to help manage pgbackrest backups.

This container has some very specific goals;

- Bundle pgbackrest.bash script and pgbackrest command inside PATH.
- Configure pgbackrest with S3 as backend.
- Run pgbackrest as ``archive_command``.
- Run pgbackrest.bash script as a scheduled task to perform periodic backups.
- Run pgbackrest.bash script as a systemd dependency to restore PGDATA from S3 if it's empty, and if a valid backup exists.

## Create stanza

When you first start this container on a new host it will fail because there is no stanza in S3. You must run ``pgbackrest.bash backup`` one time to create the stanza.

# How to upgrade major version

This must be done manually on the container host.

## Shutdown service

    systemctl --user stop postgresql15.service

## Backup pgdata volume

    podman volume export systemd-pgdata15 > systemd-pgdata15.tar

## Update postgresql container

```
mv .config/containers/systemd/pgdata15.volume .config/containers/systemd/pgdata16.volume
mv .config/containers/systemd/postgresql15.container .config/containers/systemd/postgresql16.container
vim .config/containers/systemd/postgresql16.container
...
[Container]
...
Volume=pgdata16.volume:/var/lib/postgresql/data:Z
...
```

    systemctl --user daemon-reload

## Create new pgdata volume

    podman volume create systemd-pgdata16
    podman volume import systemd-pgdata16 systemd-pgdata15.tar

## Run upgrade

    podman run --rm -v systemd-pgdata15:/var/lib/postgresql/OLD/data:Z \
        -v systemd-pgdata16:/var/lib/postgresql/NEW/data:Z \
        docker.io/tianon/postgres-upgrade:15-to-16

## Start new postgresql container

    systemctl --user start postgresql16.service

## Upgrade pgbackrest stanza

    podman exec postgresql pgbackrest stanza-upgrade
